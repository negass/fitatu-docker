FROM ubuntu:18.04

# Install dependencies
RUN apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install -y php7.2 php7.2-xml \
    php7.2-curl php7.2-json php7.2-common php7.2-cli php7.2-zip php7.2-mysql php7.2-bcmath php7.2-mbstring \
    php7.2-dev libapache2-mod-php7.2 curl apache2 git supervisor

# Install composer
RUN curl -sSL https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN chmod +x /usr/local/bin/composer

# Configure apache
RUN mkdir /var/www/recipes
COPY ./apache/apache2.conf /etc/apache2
COPY ./apache/docs.conf /etc/apache2/sites-available
COPY ./apache/recipes-prod.conf /etc/apache2/sites-available
COPY ./apache/recipes-local.conf /etc/apache2/sites-available

RUN a2ensite docs.conf
RUN a2ensite recipes-prod.conf
RUN a2ensite recipes-local.conf
RUN a2enmod rewrite
RUN a2enmod headers

# Set correct permissions
RUN usermod -u 1000 www-data
RUN chmod 775 /var/www/recipes
RUN chown www-data:www-data /var/www/recipes

# Configure PHP
COPY ./php/php.ini /etc/php/7.2/cli
COPY ./php/php.ini /etc/php/7.2/apache2

# Configure supervisord

COPY ./supervisor/apache.conf /etc/supervisor/conf.d/apache.conf
COPY ./supervisor/supervisord.conf /etc/supervisor/supervisord.conf

# Entrypoint
COPY ./entrypoint.sh /
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
