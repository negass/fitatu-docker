# Recipes project

In the first place please add hosts on local machine and create .env file from dist

* ```recipes.prod```
* ```recipes.local```
* ```docs.recipes.local``` serving swagger documentation

### Requirements

* docker
* docker-compose

### How to start

* To build containers ```docker-compose build```
* To create and start containers ```docker-compose up```
* To start existing containers ```docker-compose start```
* To stop running containers ```docker-compose stop```

### Services

* PHP 7.1.x
* Apache 2.4.x exposed on ```8181 port```
* Mysql 5.7.x exposed on ```port 3131```
* Swagger UI exposed on```port 8484```
